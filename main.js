var dsnv = [];
var DSNV = "DSNV";

var dsnvJSON = localStorage.getItem(DSNV);

if (dsnvJSON != null){
    var nvArr = JSON.parse(dsnvJSON);
    dsnv = nvArr.map(function(item){
        return new nhanVien(item.tknv,
            item.name,
            item.email,
            item.password,
            item.datepicker,
            item.luongCB,
            item.chucVu,
            item.gioLam)
    });
    renderDSNV(dsnv);
}

function themNguoiDung(){
    var nv = layThongTinTuForm();
    var isValid = true;
    isValid = kiemTraDoDai(nv.tknv, "tbTKNV", 4, 6) & kiemTraChu(nv.name, "tbTen") & kiemTraEmail(nv.email, "tbEmail") & kiemTraMatKhau(nv.password, "tbMatKhau") & kiemTraNgayLam(nv.datepicker, "tbNgay") & kiemTraLuong(nv.luongCB, "tbLuongCB", 1000000, 20000000) & kiemTraChucVu(nv.chucVu, "tbChucVu") & kiemTraSoGio(nv.gioLam, "tbGiolam", 80, 200);

    if(isValid){
        dsnv.push(nv);
        // convert dsnv array to json
        var dsnvJSON = JSON.stringify(dsnv);
        //  save data in localStorage
        localStorage.setItem(DSNV, dsnvJSON);
        renderDSNV(dsnv);
    }
}


// Xoá nhân viên
function xoaNhanVien(idNV){
    var viTri = timViTriNhanVien(idNV, dsnv);
    if (viTri != -1){
        dsnv.splice(viTri, 1);
        renderDSNV(dsnv);
    }
}

// Sửa nhân viên
function suaNhanVien(idNV){
    document.getElementById("tknv").readOnly = true;
    var viTri = timViTriNhanVien(idNV, dsnv);
    if(viTri == -1){
        return;
    }
    var nv = dsnv[viTri];
    showThongTinLenForm(nv);
}

// Cập nhật thông tin nhân viên
function capNhatNhanVien(){
    var nv = layThongTinTuForm();
    var viTri = timViTriNhanVien(nv.tknv, dsnv);
    if(viTri != -1){
        dsnv[viTri] = nv;
        renderDSNV(dsnv);
    }
}

function getClassifyFromTr(trTag){
    var tdList = trTag.querySelectorAll("td");
    return tdList[6].innerText;
}



function timLoaiNhanVien(idNV){
    var timKiem = document.getElementById("searchName").value;
    var trList = document.querySelectorAll("#tableDanhSach tr");

    for(var index = 0; index < trList.length; index++){
        var xepLoai = getClassifyFromTr(trList[index]);
        if(timKiem == "Xuất Sắc" || timKiem == "xuất sắc"){
            if(xepLoai == "Xuất sắc"){
            }
        }
    }
}
