
function kiemTraDoDai(value, idErr, min, max){
    var length = value.length;
    if(length < min || length > max){
        document.getElementById(idErr).innerText = `Độ dài phải từ ${min} - ${max} ký tự`;
        return false;
    }
    else{
        document.getElementById(idErr).innerText = "";
        return true;
    }
}

function kiemTraChu(value, idErr){
    var reg = /^[a-zA-Z ]*$/;
    var isString = reg.test(value);
    if(isString){
        document.getElementById(idErr).innerText = "";
        return true;
    }
    else{
        document.getElementById(idErr).innerText = "Trường này phải là chữ";
        return false;
    }
}

function kiemTraEmail(value, idErr){
    var reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = reg.test(value);
    if(isEmail){
        document.getElementById(idErr).innerText = "";
        return true;
    }
    else{
        document.getElementById(idErr).innerText = "Trường này phải là email";
        return false;
    }
}

function kiemTraMatKhau(value, idErr){
    var reg = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&-_])[A-Za-z\d@$!%*?&-_]{6,10}$/
    var isPassword = reg.test(value);
    if(isPassword){
        document.getElementById(idErr).innerText = "";
        return true;
    }
    else{
        document.getElementById(idErr).innerText = `Mật Khẩu từ 6 - 10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`;
        return false;
    }
}

function kiemTraNgayLam(value, idErr){
    if(value != ""){
        document.getElementById(idErr).innerText = "";
        return true;
    }
    else{
        document.getElementById(idErr).innerText = "Vui lòng nhập ngày làm";
        return false;
    }
}

function kiemTraLuong(value, idErr, min, max){
    if(value != ""){
        document.getElementById(idErr).innerText = "";
        return true;
    }
    else{
        document.getElementById(idErr).innerText = `Lương cơ bản ${min} - ${max}`;
        return false;
    }
}

function kiemTraChucVu(value, idErr){
    if(value != "Chọn chức vụ"){
        document.getElementById(idErr).innerText = "";
        return true;
    }
    else{
        document.getElementById(idErr).innerText = "Chức vụ lựa chọn không hợp lệ";
        return false;
    }
}

function kiemTraSoGio(value, idErr, min, max){
    if(value != 0){
        document.getElementById(idErr).innerText = "";
        return true;
    }
    else{
        document.getElementById(idErr).innerText = `Số giờ làm ${min} - ${max} giờ`;
        return false;
    }
}