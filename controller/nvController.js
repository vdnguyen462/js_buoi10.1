function layThongTinTuForm(){
    const _tknv = document.getElementById("tknv").value;
    const _name = document.getElementById("name").value;
    const _email = document.getElementById("email").value;
    const _password = document.getElementById("password").value;
    const _datepicker = document.getElementById("datepicker").value;
    const _luongCB = document.getElementById("luongCB").value;
    const _chucVu = document.getElementById("chucvu").value;
    const _gioLam = document.getElementById("gioLam").value;

    var nv = new nhanVien(_tknv, 
        _name, 
        _email,
        _password, 
        _datepicker, 
        _luongCB, 
        _chucVu, 
        _gioLam)
    return nv;
}

function renderDSNV(nvArr){
    var contentHTML = "";
    const formatter = new Intl.NumberFormat('vn-VN', {
        style: 'currency',
        currency: 'VND',
      });
    for (var index = 0; index < nvArr.length; index++){
        var item = nvArr[index];
        var contentTr = `<tr>
        <td>${item.tknv}</td>
        <td>${item.name}</td>
        <td>${item.email}</td>
        <td>${item.datepicker}</td>
        <td>${item.chucVu}</td>
        <td>${formatter.format(item.tinhLuong())}</td>
        <td>${item.xepLoai()}</td>
        <td>
            <button onclick = "xoaNhanVien('${item.tknv}')"
            class="btn btn-danger">Xoá</button>
            <button onclick = "suaNhanVien('${item.tknv}')"
            class="btn btn-success" id="btnSua"
            data-toggle="modal"
            data-target="#myModal">Sửa</button>
        </td>
        </tr>`
        contentHTML +=contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

// Tìm vị trí

function timViTriNhanVien(id, arr){
    var viTri = -1;
    for(var index = 0; index < arr.length; index++){
        var nv = arr[index];
        if(nv.tknv == id){
            viTri = index;
            break;
        }
    }
    return viTri;
}

// Show thông tin lại lên form

function showThongTinLenForm(nv){
    document.getElementById("tknv").value = nv.tknv;
    document.getElementById("name").value = nv.name;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.password;
    document.getElementById("datepicker").value = nv.datepicker;
    document.getElementById("luongCB").value = nv.luongCB;
    document.getElementById("chucvu").value = nv.chucVu; 
    document.getElementById("gioLam").value = nv.gioLam;
}

