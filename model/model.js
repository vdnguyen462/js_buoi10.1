function nhanVien(_tknv, 
    _name, 
    _email, 
    _password, 
    _datepicker, 
    _luongCB, 
    _chucVu, 
    _gioLam){
    this.tknv = _tknv;
    this.name = _name;
    this.email = _email;
    this.password = _password;
    this.datepicker = _datepicker;
    this.luongCB = _luongCB;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;
    this.tinhLuong = function(){
        if(this.chucVu == "Sếp")
            return this.luongCB * 3;
        else if (this.chucVu == "Trưởng phòng")
            return this.luongCB * 2;
        else if (this.chucVu == "Nhân viên")
            return this.luongCB;
    }
    this.xepLoai = function(){
        if(this.gioLam >= 192)
            return "Xuất sắc"
        else if (this.gioLam >= 176 && this.gioLam < 192)
            return "Giỏi"
        else if (this.gioLam >= 160 && this.gioLam < 176)
            return "Khá"
        else
            return "Trung Bình"
    }
}